
/**
 * Exercises inspired from https://github.com/appalaszynski/javascript-exercises
 * An intro to ramda and general notes can be found at https://www.sseghers.tech/ramda
 *
 * All exercises are found under /exercises. Each exercise will follow this structure:
 *  <exercise-name>.js - Explanation of the problem is provided here. You may place your solution here as well, or another file.
 *  <exercise-name>-solution.js - A possible soluiton provided for reference. I also provide some notes on reasoning and intent.
 *  <exercise-name>.test.js - The test file for use in your solution(s). Import the file containing your implementation
 *      and run it with either script test:answers or script test:unit which can be modified to watch individual spec files.
**/
