import { R } from '../common';
const { reduceRight } = R;

function localReverse(input) {
  return reduceRight((value, acc) => acc += value, '', [...input]);
}

/*
  A straightforward problem with many solutions. reduceRight works great for iterating collections in reverse. I did not name it reverse to avoid
  conflicts with ramda's own reverse. Whether or not you, the implementer, consider that cheating is at your discretion.
*/

export { localReverse };
