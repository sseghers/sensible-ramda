import { expect } from 'chai';

import { Queue } from './queue';

describe('Queue', function() {
  it('should be a function type', function() {
    expect(typeof Queue).to.equal('function');
  });

  it('should add a number', function() {
    const queue = new Queue();

    const addResult = queue.add.bind(5);

    expect(addResult).not.to.throw();
  });

  it('should remove', function() {
    const queue = new Queue();

    queue.add(5);

    expect(queue.remove).not.to.throw();
  });

  it('should remove and return a number', function() {
    const queue = new Queue();

    queue.add(5);

    expect(queue.remove()).to.equal(5);
  });

  it('should remove and return the first number added', function() {
    const queue = new Queue();

    queue.add(5);
    queue.add(10);

    expect(queue.remove()).to.equal(5);
  });

  it('should not crash when all numbers have been removed', function() {
    const queue = new Queue();

    queue.add(10);
    queue.add(20);

    expect(queue.remove()).to.equal(10);
    expect(queue.remove()).to.equal(20);
    expect(queue.remove).to.not.throw();
  });
});







// * Examples:
//  * const q = new Queue();
//  * q.add(1);
//  * q.remove(); // returns 1
