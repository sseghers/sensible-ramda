import { R } from '../common';
const {
  compose,
  concat,
  converge,
  head,
  join,
  map,
  split,
  tail,
  toUpper,
} = R;

const capitalizeWord = converge(concat, [compose(toUpper, head), tail]);
const capitalize = compose(join(' '), map(capitalizeWord), split(' '));

/*
  Straightforward approach. Split the string, operate on every word, join it back for the final result. The feature function is capitalizeWord which
  essentially applies two operations, one uppercasing the first word pulled with head, the other taking the rest of the word with tail.
  From there, converge feeds those results into concat to join them.
*/

export { capitalize };
