import { expect } from 'chai';

import { R } from '../common';
import { steps } from './steps';

const {
  add,
  reduce,
  prop,
} = R;

describe('Steps solution', function() {
  const getExpectedResults = level => {
    return [level, level * level];
  }
  const sumLength = reduce((acc, val) => add(acc, prop('length', val)), 0);
  it('should return lines for 2 levels', function() {
    const [expectedLines, expectedLength] = getExpectedResults(2);

    const result = steps(2);
    const totalLength = sumLength(result);

    expect(result).to.have.length(expectedLines);
    expect(totalLength).to.equal(expectedLength);
  });

  it('should return lines for 4 levels', function() {
    const [expectedLines, expectedLength] = getExpectedResults(4);

    const result = steps(4);
    const totalLength = sumLength(result);
    expect(result).to.have.length(expectedLines);
    expect(totalLength).to.equal(expectedLength);
  });

  it('should return lines for 10 levels', function() {
    const [expectedLines, expectedLength] = getExpectedResults(10);

    const result = steps(10);
    const totalLength = sumLength(result);
    expect(result).to.have.length(expectedLines);
    expect(totalLength).to.equal(expectedLength);
  });
})
