import { expect } from 'chai';

import { chunk } from './chunk';

describe('Chunk problems', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('[1, 2, 3, 4], 2'), function() {
    expect(chunk([1, 2, 3, 4], 2)).to.deep.equal([[ 1, 2], [3, 4]]);
  });

  it(createCriteriaFromInput('[1, 2, 3, 4, 5], 2'), function() {
    expect(chunk([1, 2, 3, 4, 5], 2)).to.deep.equal([[ 1, 2], [3, 4], [5]]);
  });

  it(createCriteriaFromInput('[1, 2, 3, 4, 5, 6, 7, 8], 3'), function() {
    expect(chunk([1, 2, 3, 4, 5, 6, 7, 8], 3)).to.deep.equal([[ 1, 2, 3], [4, 5, 6], [7, 8]]);
  });

  it(createCriteriaFromInput('[1, 2, 3, 4, 5], 4'), function() {
    expect(chunk([1, 2, 3, 4, 5], 4)).to.deep.equal([[ 1, 2, 3, 4], [5]]);
  });

  it(createCriteriaFromInput('[1, 2, 3, 4, 5], 10'), function() {
    expect(chunk([1, 2, 3, 4, 5], 10)).to.deep.equal([[ 1, 2, 3, 4, 5]]);
  });
});



