import { expect } from 'chai';

import { capitalize } from './capitalize';


describe('Capitalize problems', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('a short sentence'), function() {
    expect(capitalize('a short sentence')).to.equal('A Short Sentence');
  });

  it(createCriteriaFromInput('a lazy fox'), function() {
    expect(capitalize('a lazy fox')).to.equal('A Lazy Fox');
  });

  it(createCriteriaFromInput('look, it is working!'), function() {
    expect(capitalize('look, it is working!')).to.equal('Look, It Is Working!');
  });
});
