import { log, R } from '../common';
const {
  __,
  add,
  always,
  compose,
  curry,
  flip,
  inc,
  divide,
  join,
  map,
  multiply,
  prop,
  subtract,
  times,
  identity,
} = R;

const padStart = curry((source, count) => source.padStart(count, ' '));

const padEnd = curry((source, count) => source.padEnd(count, ' '));

const nthOddNumber = compose(subtract(__, 1), multiply(2));

const spacesLength = input => subtract(nthOddNumber(input));

const makePyramidLine = compose(join(''), times(always('#')), nthOddNumber);

const formatLine = curry((spacesLengthFromInput, line) => {
  const lineLength = prop('length', line);
  const linesCount = spacesLengthFromInput(lineLength);
  if(!linesCount) {
    return line;
  }
  const startCount = divide(linesCount, 2);
  const endCount = multiply(2, startCount);
  return compose(flip(padEnd)(add(lineLength, endCount)), flip(padStart)(add(lineLength, startCount)))(line);
});

function pyramid(input) {
  const formatLineWithSpace = formatLine(spacesLength(input));
  return compose(
    map(compose(identity, formatLineWithSpace, makePyramidLine)),
    times(inc)
  )(input);
}

/*
Definitely an interesting challenge despite its conceptual simplicity. I create an array which contains the consecutive integers of the input.
Each number is fed to makePyramidLine to create the string containing the hashes. formatLineWithSpace, using the input argument, calculates
the number of spaces needed based on the hash string content, then pads it with a character of choice from the padStart and padEnd functions.
*/

export { pyramid };
