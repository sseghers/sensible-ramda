import { R } from '../common';
const {
  __,
  compose,
  gt,
  ifElse,
  juxt,
  multiply,
  negate,
  nth,
  reverse,
  toString,
} = R;

function reverseInt(input) {
  const isNegative = compose(gt(__, 0), multiply(-1));
  const reverseNumber = compose(Number, reverse, toString, Math.abs);
  return compose(ifElse(nth(0), compose(negate, nth(1)), nth(1)), juxt([isNegative, reverseNumber]))(input);
}

/*
  This is ugly because of my ignorance in including preevaluated expressions with compose. Here I simply need
  to evaluate the output of reverseNumber against isNegative, where the latter points to the input as is, and the former
  does what it's asked; reverse the number.

  I could also use a new function that ignores the final composed input for isNegative like the following:
    const isNegative = usedInput => ignoredInput => compose(gt(__, 0), multiply(-1))(usedInput);

  However I'd like to keep my first attempt for learning purposes.
*/

export { reverseInt };
