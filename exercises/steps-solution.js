import { R } from '../common';

const {
  add,
  concat,
  curry,
  join,
  repeat,
  subtract,
  times,
} = R;

const getLine = curry((max, current) => {
  const plusOne = add(1, current);
  return concat(join('', repeat('#', plusOne)), join('', repeat(' ', subtract(max, plusOne))));
})


function steps(levels) {
  return times(getLine(levels), levels);
}

/*
  Times drives through all sequential numbers, and the curried getLine captures the max number, and each line. From there I combine two strings.
  One generates the current line's hashes, the other the remaining spaces for that line.
*/

 export { steps };
