import { expect } from 'chai';

import { isEven } from './is-even';

describe('IsEven', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('4'), function() {
    expect(isEven(4)).to.equal(true);
  });

  it(createCriteriaFromInput('3'), function() {
    expect(isEven(3)).to.equal(false);
  });

  it(createCriteriaFromInput('42'), function() {
    expect(isEven(42)).to.equal(true);
  });
});
