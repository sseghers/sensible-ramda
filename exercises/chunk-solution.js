import { R } from '../common';
const {
  __,
  add,
  always,
  append,
  compose,
  divide,
  prop,
  equals,
  or,
  reduce,
  slice,
  times,
  until,
} = R;

const getChunks = compose(times(always([])), compose(Math.ceil, divide));
const totalArrayMatchesChunksSize = chunks => totalArray => equals(prop('length', chunks), prop('length', totalArray))

function chunk(input, size) {
  const chunks = getChunks(input.length, size);
  const matchesSize = totalArrayMatchesChunksSize(chunks);
  const totalLength = reduce((acc, val) => add(compose(or(__, 0), prop('length'))(val), acc), 0);
  const retrieveNextChunk = (input, size, acc) => {
    const accLength = totalLength(acc);
    return slice(accLength, add(accLength, size), input);
  }
  return until(matchesSize, (acc) => append(retrieveNextChunk(input, size, acc), acc))([]);
}
/*
  I create as many arrays as there will be in total using Math.ceil with getChunks. The size of chunks is used to determine our stop condition
  for until which compares that and the running accumulator array length. retrieveNextChunk reduces to calculate the length which tells me the
  number of chunks currently held. For each successive chunk, the length of the items held serves as the next index added to the size of each chunk.
  Whew.
*/

export { chunk };
