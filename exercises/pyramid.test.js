import { expect } from 'chai';

import { pyramid } from './pyramid';

describe('Pyramid', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput(1), function() {
    expect(pyramid(1)).to.deep.equal(['#']);
  });

  it(createCriteriaFromInput(2), function() {
    expect(pyramid(2)).to.deep.equal([' # ', '###']);
  });

  it(createCriteriaFromInput(3), function() {
    expect(pyramid(3)).to.deep.equal(['  #  ', ' ### ', '#####']);
  });
});
