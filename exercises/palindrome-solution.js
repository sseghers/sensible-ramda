import { R } from '../common';
const {
  converge,
  equals,
  identity,
  reverse,
} = R;

const palindrome = converge(equals, [identity, reverse]);

/*
  Yet another clear example of the Ramda tooling doing work for me. Again, converge receives output from both functions, which also both receive the same input.
  The result from identity(itself) is compared to the result from reverse.
*/

export { palindrome };
