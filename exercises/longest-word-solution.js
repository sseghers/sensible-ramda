import { R } from '../common';
const {
  compose,
  nth,
  sort,
  split,
} = R;

const longestWord = compose(nth(0), sort((a, b) => b.length - a.length), split(/[^a-z, A-Z]+|\s/));

/*
  I split the input matching on not letters, then sort them based on length. The first element is the target word.
  The regex pattern used here could probably be improved, but I suggest keeping focus on the problem over the potential risk of entering the Regex Effect.
*/

export { longestWord };
