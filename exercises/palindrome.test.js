import { expect } from 'chai';
import { palindrome } from './palindrome';

describe('Palindrome', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('abba'), function() {
    expect(palindrome('abba')).to.equal(true);
  });

  it(createCriteriaFromInput('abcdefg'), function() {
    expect(palindrome('abcdefg')).to.equal(false);
  });
});
