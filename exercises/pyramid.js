/**
 * Pyramid
 *
 * Write a function that accepts a positive number N.
 * The function should return a pyramid shape
 * with N levels using the # character. Note the extra padding for each level
 * before the final one.
 *
 * Examples:
 * pyramid(1) = '#'
 *
 * pyramid(2) = ' # '
 *              '###'
 *
 * pyramid(3) = '  #  '
 *              ' ### '
 *              '#####'
 *
 */
