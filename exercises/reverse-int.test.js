import { expect } from 'chai';

import { reverseInt } from './reverse-int';

describe('reverseInt', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput(-40), function() {
    expect(reverseInt(-40)).to.equal(-4);
  });

  it(createCriteriaFromInput(15), function() {
    expect(reverseInt(15)).to.equal(51);
  });

  it(createCriteriaFromInput(981), function() {
    expect(reverseInt(981)).to.equal(189);
  });


  it(createCriteriaFromInput(500), function() {
    expect(reverseInt(500)).to.equal(5);
  });

  it(createCriteriaFromInput(-15), function() {
    expect(reverseInt(-15)).to.equal(-51);
  });

  it(createCriteriaFromInput(-90), function() {
    expect(reverseInt(-90)).to.equal(-9);
  });
});
