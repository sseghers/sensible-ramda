import { expect } from 'chai';

import { anagrams } from './anagrams';

describe('Anagrams problems', function() {
  const createCriteriaFromInput = (stringOne, stringTwo) => `Passes with input: ${stringOne} - ${stringTwo}`;

  it(createCriteriaFromInput('rail safety', 'fairy tales'), function() {
    expect(anagrams('rail safety', 'fairy tales')).to.be.true;
  });

  it(createCriteriaFromInput('RAIL! SAFETY!', 'fairy tales'), function() {
    expect(anagrams('RAIL! SAFETY!', 'fairy tales')).to.be.true;
  });

  it(createCriteriaFromInput('Hi there', 'Bye there'), function() {
    expect(anagrams('Hi there', 'Bye there')).to.be.false;
  });
});

