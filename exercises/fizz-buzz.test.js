import { expect } from 'chai';

import { fizzBuzz } from './fizz-buzz';

describe('FizzBuzz', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('5'), function() {
    const result = fizzBuzz(5);
    expect(result).to.deep.equal([1,2,'fizz', 4, 'buzz']);
  });

  it(createCriteriaFromInput('15'), function() {
    const result = fizzBuzz(15);
    expect(result).to.deep.equal([1,2,'fizz', 4, 'buzz', 'fizz',7,8,'fizz','buzz',11,'fizz',13,14,'fizzbuzz']);
  });
});

