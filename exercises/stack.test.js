import { expect } from 'chai';

import { Stack } from './stack';

describe('Stack solution', function() {
  it('should be empty on creation', function() {
    const expectedResult = [];
    const stackInstance = new Stack();

    expect(stackInstance.tap()).eql(expectedResult);
  });

  describe('when pushed with an element', function() {
    it('should contain only the element', function() {
      const targetElement = 5;

      const stackInstance = new Stack();
      stackInstance.push(targetElement);

      expect(stackInstance.tap()).eql([targetElement]);
    });

    it('should return the last pushed item on peek without side effects', function() {
      const targetElement = 6;

      const stackInstance = new Stack();
      stackInstance.push(targetElement);
      const result = stackInstance.peek();

      expect(result).to.equal(targetElement);
      expect(stackInstance.tap()).eql([targetElement]);
    });

    it('should return the last pushed item on pop and reflect the effect', function() {
      const targetElement = 'foo';

      const stackInstance = new Stack();
      stackInstance.push(targetElement);
      const result = stackInstance.pop();

      expect(result).equal(targetElement);
      expect(stackInstance.tap()).eql([]);
    });

    it('should return the item pushed item as is', function() {
      const targetElement = [1,2,3,4,5];

      const stackInstance = new Stack();
      stackInstance.push(targetElement);

      expect(stackInstance.tap()).eql([targetElement]);
    });
  });

  describe('when pushed with multiple elements at once', function() {
    it('should contain only those elements', function() {
      const targetElement = [1,2,3];

      const stackInstance = new Stack();
      stackInstance.push(...targetElement);

      expect(stackInstance.tap()).eql(targetElement);
    });

    it('should pop elements in stack order', function() {
      const targetElement = [1,2,3];

      const stackInstance = new Stack();
      stackInstance.push(...targetElement);

      expect(stackInstance.pop()).equal(3);
      expect(stackInstance.pop()).equal(2);
      expect(stackInstance.pop()).equal(1);
      expect(stackInstance.peek()).equal(undefined);
    });
  });

  describe('when no items are pushed', function() {
    it('should not throw an error on pop or peek', function() {
      const stackInstance = new Stack();

      expect(stackInstance.pop).to.not.throw();
      expect(stackInstance.peek).to.not.throw();
    });
  });
});
