import { log, R } from '../common';
const {
  and,
  compose,
  cond,
  identity,
  inc,
  map,
  not,
  T,
  tail,
  times,
  juxt,
  all,
} = R;

const mod = divisor => dividend => dividend % divisor;
const isModThree = compose(not, mod(3));
const isModFive = compose(not, mod(5));
const isModThreeAndFive = compose(all(R.equals(true)), juxt([isModThree, isModFive]));
const fizzBuzz = compose(
  map(cond([
    [isModThreeAndFive, () => 'fizzbuzz'],
    [isModThree, () => 'fizz'],
    [isModFive, () => 'buzz'],
    [T, identity]
  ])),
  tail,
  times(identity),
  inc);


/*
  There are at least two main routes that could be used here. One option could make more use of juxt which would run all three conditions on each number, allowing
  you to return the appropriate result based on the booleans for each array(per number). The other route, used here, leverages cond which functions like a switch
  statement. The order of each conditional operation matters here. Were isModThree to be listed first, isModThreeAndFive would never be reached. The overhead happening
  at the start of the compose is to generate an array of numbers starting with 1 and ending with the number.

*/

export { fizzBuzz };
