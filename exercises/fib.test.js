import { expect } from 'chai';

import { fib } from './fib';

describe('Chunk problems', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('4'), function() {
    expect(fib(4)).to.equal(3);
  });

  it(createCriteriaFromInput('15'), function() {
    expect(fib(15)).to.equal(610);
  });
});
