import { expect } from 'chai';

import { localReverse } from './reverse-string';

describe('reverseString', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('apple'), function() {
    expect(localReverse('apple')).to.equal('elppa');
  });

  it(createCriteriaFromInput('hello'), function() {
    expect(localReverse('hello')).to.equal('olleh');
  });

  it(createCriteriaFromInput('Greetings!'), function() {
    expect(localReverse('Greetings!')).to.equal('!sgniteerG');
  });
});
