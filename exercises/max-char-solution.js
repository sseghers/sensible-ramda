import { R } from '../common';
const {
  nth,
  compose,
  sort,
  countBy,
  toPairs,
  identity,
} = R;

const maxChar = compose(
  nth(0),
  nth(0),
  sort((a, b) => b[1] - a[1]),
  toPairs,
  countBy(identity),
  Array.from,
  );

/*
  I could not help give a laugh when finishing this one. The functions available made it plainly easy to get what I needed. Given the string, it converts it to an array
  characters which is fed into countBy. This gets me the character and occurrence count, which is then convert as an array of arrays. Using this array, I sort
  by the count, and grab the first entry's key which is the character.
*/

export { maxChar };
