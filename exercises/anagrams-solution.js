import { R } from '../common';
const {
  compose,
  countBy,
  identity,
  equals,
  replace,
  toLower,
} = R;


const getWordCount = compose(countBy(identity), Array.from, toLower, replace(/\W/g, ''));
const anagrams = (firstInput, secondInput) => equals(getWordCount(firstInput), getWordCount(secondInput));

/*
  You will see compose used often and throughout my answers, although pipe could also be used.
  Since the solution specifies ignoring spaces and special characters, I first remove those via regex. From that result I lower case it to ignore casing.
  Then prepare it as an array to pass through countBy which yields an object with keys containing our characters and values reflecting the occurrences.
  This approach is used on both string inputs to check for equality.
  Notice that my function, getWordCount, is used twice here and, while I could be extra clever with some way to replicate it and provide to equals, I
  specifically keep it simple for readability.
*/

export { anagrams };
