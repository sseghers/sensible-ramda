import { R } from '../common';
const {
  and,
  compose,
  reduce,
  converge,
} = R;

const fillArray = (length) => Array(length).fill();
const isEvenNumber = compose(reduce((acc) => {
  acc = !acc;
  return acc;
}, true), fillArray);

const isEven = converge(and, [Boolean, isEvenNumber]);

/*
  This one is a little tricky. The first boolean check is eliminate zero. The scond function generates an array matching the input in length,
  then simply flips between true and false N times until the final result is reached. I combine the output from both.
*/

export { isEven };
