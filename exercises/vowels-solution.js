import { log, R } from '../common';

const {
  __,
  compose,
  filter,
  includes,
  prop,
  split,
  toLower,
} = R;

const vowels = compose(prop('length'), filter(includes(__, ['a', 'e', 'i', 'o', 'u'])), split(''), toLower);

/*
  I utilize existing ramda functions to say exactly what is needed. toLower ignores the casing. Then the input is convert to an array.
  From there I can filter in vowels and count that string result.
*/
export { vowels };
