import { gather, R } from '../common';

const {
  compose,
  concat,
  flatten,
  head,
  identity,
  isEmpty,
  juxt,
  lens,
  set,
  tail,
  unless,
  view,
} = R;


const printLens = lens(identity, identity);
const printArray = view(printLens);
const print = (memory) => printArray(memory);

const queueLens = lens(() => null, concat);

const decapitate = unless(isEmpty, juxt([head, tail]));

function Queue() {
  let memory = [];

  this.print = () => print(memory);

  this.add = (...args) => {
    memory = compose(set(queueLens, memory), flatten, gather)(args);
  }

  this.remove = () => {
    const [head, tail] = decapitate(memory);
    memory = tail;
    return head;
  }
}

/*
  There is an interesting clash to note here. In a real world scenario, you could find instances of code that require side effects in the form of state
  bound by closures. Queue, my function handling the behavior, internally tracks some state to be useful as a queue. I do not want to expose it externally, hence
  I did not place it on its this instance.
  Aside from that, I thought lens to be an interesting choice here. I like the abstraction it provides via the finite control of read and write operations.
  If Queue needed it's core add functionality to change, the lens could be changed for another.
*/

export { Queue }
