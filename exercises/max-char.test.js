import { expect } from 'chai';
import { maxChar } from './max-char';

describe('Max Char', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('abcccccccd'), function() {
    expect(maxChar('abcccccccd')).to.equal('c');
  });

  it(createCriteriaFromInput('apple 1231111'), function() {
    expect(maxChar('apple 1231111')).to.equal('1');
  });

  it(createCriteriaFromInput('foa1cadb3a'), function() {
    expect(maxChar('foa1cadb3a')).to.equal('a');
  });
});
