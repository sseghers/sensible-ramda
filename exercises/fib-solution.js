import { R, gather } from '../common';
const {
  __,
  compose,
  concat,
  converge,
  equals,
  identity,
  last,
  prop,
  sum,
  takeLast,
  until,
} = R;

const lengthMatchesIndex = input => compose(equals(__, input), prop('length'));
const seed = [1, 1];
const sumNext = compose(gather, sum, takeLast(2));
function fib(input) {
  const inputLengthMatchesIndex = lengthMatchesIndex(input);
  const createFibs = until(inputLengthMatchesIndex, converge(concat, [identity, sumNext]));

  return compose(last, createFibs)(seed);
}

/*
  This solution utilizes a loop approach rather than recursion. First, I create a function capturing the input which is the target number we use to
  calculate the result at it's index value. It's used as the stopping condition for until, comparing the next array containing the total fibonacci sequence.
  When the length matches the sequence index in question, the result has been reached. The converge is a handy way to remain point free; concating the number it
  receives to the sum of the next two numbers. What's returned is the application of the "seed", or first two numbers, to this process where the final result is
  plucked from the tail.
*/

export { fib };
