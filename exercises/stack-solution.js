import { R } from '../common';
const {
  compose,
  concat,
  flip,
  head,
  identity,
  isEmpty,
  lens,
  set,
  view,
} = R;


const tapLens = lens(identity, identity);
const viewStack = view(tapLens);

const stackLens = lens(() => null, flip(concat));

const peekLens = lens(identity, identity);

function Stack() {
  let memory = [];

  this.tap = () => viewStack(memory);

  this.push = (...args) => {
    memory = compose(set(stackLens, memory))(args);
  }

  this.peek = () => {
    if(isEmpty(memory)) {
      return;
    }
    return compose(head, view(peekLens))(memory);
  }

  this.pop = () => {
    if(isEmpty(memory)) {
      return;
    }
    const last = memory.slice(-1);
    memory = memory.slice(0, -1);
    return head(last);
  }
}

/*
  The concerns and solution are very similar to Queue. I am mixing side effect driven state with functional ramda to see what compromise can be reached.
  Lens is used to control how the internal state is accessed.
*/

export { Stack }
