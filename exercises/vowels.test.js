import { expect } from 'chai';

import { vowels } from './vowels';

describe('Vowels solution', function() {
  it('should return the correct length for sample 1', function() {
    const input = 'aeiou';

    const result = vowels(input);

    expect(result).to.equal(5);
  });

  it('should return the correct length for sample 2', function() {
    const input = 'Adam';

    const result = vowels(input);

    expect(result).to.equal(2);
  });

  it('should return the correct length for sample 3', function() {
    const input = 'Hello there!';

    const result = vowels(input);

    expect(result).to.equal(4);
  });
})
