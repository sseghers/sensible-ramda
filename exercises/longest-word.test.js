import { expect } from 'chai';

import { longestWord } from './longest-word';

describe('Longest word', function() {
  const createCriteriaFromInput = (stringOne) => `Passes with input: ${stringOne}`;

  it(createCriteriaFromInput('Hello there'), function() {
    expect(longestWord('Hello there')).to.equal('Hello');
  });

  it(createCriteriaFromInput('My name is Adam'), function() {
    expect(longestWord('My name is Adam')).to.equal('name');
  });

  it(createCriteriaFromInput('fun&!! time'), function() {
    expect(longestWord('fun&!! time')).to.equal('time');
  });

  it(createCriteriaFromInput('@43423N BAR'), function() {
    expect(longestWord('@43423N BAR')).to.equal('BAR');
  });
});
