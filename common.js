import * as R from 'ramda';

const log = console.log;

const gather = (...args) => args;
const spread = (args) => [...args];

export {
  gather,
  log,
  R,
  spread,
};
