import { log, R } from '../common';
import BIG_DATA from '../concepts/MOCK_DATA.json';
const {
  compose,
  filter,
  map,
  transduce,
} = R;


const SMALL_DATA = [
  {"id":1,"last_name":"Learie","email":"plearie0@comsenz.com","gender":"Female","ip_address":"57.195.62.150"},
  {"id":2,"last_name":"Pollen","email":"fpollen1@de.vu","gender":"Female","ip_address":"74.74.228.44"},
  {"id":3,"last_name":"Muncaster","email":"mmuncaster2@woothemes.com","gender":"Female","ip_address":"119.3.57.93"}];


function lastNameStartsWith(pattern) {
  return function(data) {
    return data.last_name.startsWith(pattern);
  }
}

function ipIsMaxCount(data) {
  return data.ip_address.match(/^([0-9]+)./)[1].length === 3;
}

function addDerivedName(data) {
  return {
    ...data,
    derived_name: `${data.last_name}, ${data.email}`,
  };
}

const runWithTimerOutput = (timerLabel, operations, logResult = false) => {
  console.time(timerLabel);

  if(logResult) {
    console.log("Results: ", operations());
  } else {
    operations();
  }

  console.timeEnd(timerLabel);
}

// Standard approach with filter + map
const smallDataStandardTimer = 'Sample standard solution.';
// runWithTimerOutput(
//   smallDataStandardTimer,
//   () => SMALL_DATA
//     .filter(lastNameStartsWith('M'))
//     .filter(ipIsMaxCount)
//     .map(addDerivedName), true);
const bigDataStandardTimer = 'Big standard solution.';
// runWithTimerOutput(
//   bigDataStandardTimer,
//   () => BIG_DATA
//     .filter(lastNameStartsWith('M'))
//     .filter(ipIsMaxCount)
//     .map(addDerivedName));



// Transduce approach
function filterReducer(predicate) {
  return function(combiner) {
    return function(source, value) {
      if(predicate(value)) {
        return combiner(source, value)
      }
      return source;
    }
  }
}

function mapReducer(mapper) {
  return function(combiner) {
    return function(source, value) {
      return combiner(source, mapper(value));
    }
  }
}

function dataCombiner(data, newData) {
  data.push(newData);
  return data;
};

const lastNameReducer = filterReducer(lastNameStartsWith('M'));
const ipLengthReducer = filterReducer(ipIsMaxCount);
const derivedNameReducer = mapReducer(addDerivedName);

const dataTransduce = compose(lastNameReducer, ipLengthReducer, derivedNameReducer)(dataCombiner);

const smallDataTransduceTimer = "Small data transduce solution.";
// runWithTimerOutput(smallDataTransduceTimer, () => SMALL_DATA.reduce(dataTransduce, []));
const bigDataTransduceTimer = "Big data transduce solution.";
// runWithTimerOutput(bigDataTransduceTimer, () => BIG_DATA.reduce(dataTransduce, []));



// Ramda transduce
const operationsRamda = compose(
  filter(lastNameStartsWith('M')),
  filter(ipIsMaxCount),
  map(addDerivedName),
);

const smallDataRamda = "Small data ramda solution.";
// runWithTimerOutput(smallDataTransduceTimer, () => transduce(operationsRamda, dataCombiner, [], SMALL_DATA));
const bigDataRamda = "Big data ramda solution.";
// runWithTimerOutput(bigDataRamda, () => transduce(operationsRamda, dataCombiner, [], BIG_DATA));
